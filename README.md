# Grasscutter Resources
> <em>Resource Version - "Certain Anime Game" 3.2</em><br/>

- ExcelBinOutput (QuestExcelConfigData 3.1 but miss acceptCond,failCond,finishExec,failExec,beginExec)
- BinOutput (2.7 base) (3.1 configGadget,AbilityGroup,Ability,ConfigAvatar,Climate,Common,Coop,CustomLevelBrick,CustomLevelDungeon,HomeFurniture,HomeworldFurnitureSuit,Monster,Scene/LevelLayout,SceneNpcBornNoGroup,Talent,Quest) (LevelDesign\Routes ??)
- Scripts (2.6.x) (Scene,Quest Shareconfig,Activity,Common 3.1)
- Readable (??)
- Subtitle (??)
- TextMap (??)
- QuestEncryptionKeys (3.1)

## Learn LUA
 How Dungeons work:
- [Spire of Solitary Enlightenment - LV 4](Resources/Scripts/Scene/40653/scene40653_group240653001.lua)
## Problem:
- Make Quest 2.8-3.1 work with modified QuestExcelConfigData which you can get from BinOutput/Quest/*.json. [use binout instead of excelconfig](https://github.com/Hartie95/Grasscutter/commit/0284de81563d30afb81733d7a3523a97419eb977)
- [Missing Scripts](https://github.com/Hartie95/Grasscutter/wiki/missing-scripts) should have been resolved by using latest data from "Script/Common" folder so let's wait for latest update from him ;).
## Problem Solved:
- Natural Spawn for Sumeru area is available.
- Quest should have worked automatically continued when quest was completed (if using [Hartie95 fork](https://github.com/Hartie95/Grasscutter/wiki/The-Outlander-Who-Caught-the-Wind-(Prologue-Act-1)))

## Credits 
 - [Dimbreath](https://github.com/Dimbreath) (Everything except Script,BinOutput) <br/>
 - [tamilpp25](https://github.com/tamilpp25/Grasscutter_Resources) (Script,BinOutput) <br/> 
 - [timing1337](https://github.com/timing1337/GenshinData) (3.1 BinOutput) <br/>
 - [Koko-boya](https://github.com/Koko-boya) (Original Owner Grasscutter_Resources) <br/>
 - [lilmayofuksu](https://github.com/lilmayofuksu/animepython) (Script aka Lua)<br/>
 - [GI-cutscenes](https://github.com/ToaHartor/GI-cutscenes/) (Key Quest) <br/>
 - [MTAlexKen](https://github.com/MTAlexKen/Genshin-resources) (Routes)<br/>
 - [radioegor146](https://github.com/radioegor146) <br/>
 - [TheLostTree](https://github.com/TheLostTree) <br/>